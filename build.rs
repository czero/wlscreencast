use std::env::var;
use std::path::Path;

use wayland_scanner::{generate_code, Side};
pub fn main() {
    pkg_config::Config::new()
        .atleast_version("1.16.0")
        .probe("gstreamer-allocators-1.0")
        .unwrap();

    let protocol_file = "./protocol/protocol_wlr-export-dmabuf-unstable-v1.xml";

    let out_dir = var("OUT_DIR").unwrap();
    let out_dir = Path::new(&out_dir);

    generate_code(
        protocol_file,
        out_dir.join("wlr_export_dmabuf_api.rs"),
        Side::Client,
    );
}
