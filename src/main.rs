use std::os::unix::io::{FromRawFd, RawFd};

use crossbeam_channel::{unbounded, Receiver, Sender, TryRecvError};

use generated::client::{
    zwlr_export_dmabuf_frame_v1::{Event as FrameEvent, ZwlrExportDmabufFrameV1},
    zwlr_export_dmabuf_manager_v1::ZwlrExportDmabufManagerV1,
};
use wayland_client::{protocol::wl_output::*, *};

use gstreamer::{prelude::*, Buffer, ClockTime, Pipeline};
use gstreamer_app::{AppSrc, AppSrcCallbacks};
use gstreamer_sys::{GstAllocator, GstMemory};
use gstreamer_video::{VideoFormat, VideoInfo};

extern "C" {
    fn gst_dmabuf_allocator_new() -> *mut GstAllocator;
    fn gst_dmabuf_allocator_alloc(
        alloc: *mut GstAllocator,
        fd: RawFd,
        size: usize,
    ) -> *mut GstMemory;
}

#[derive(Default)]
struct FrameData {
    width: u32,
    height: u32,
    tv_sec_hi: u32,
    tv_sec_lo: u32,
    tv_nsec: u32,
    fd: RawFd,
    size: usize,
}

impl FrameData {
    fn get_pts(&self) -> ClockTime {
        ClockTime::from_seconds((self.tv_sec_hi as u64) << 32)
            + ClockTime::from_seconds(self.tv_sec_lo as u64)
            + ClockTime::from_nseconds(self.tv_nsec as u64)
    }
    fn to_gst_buffer(&self, alloc: *mut GstAllocator) -> Buffer {
        let mut buf = Buffer::new();
        unsafe {
            gstreamer_sys::gst_buffer_append_memory(
                buf.as_mut_ptr(),
                gst_dmabuf_allocator_alloc(alloc, self.fd, self.size),
            );
        }
        buf.get_mut().unwrap().set_pts(self.get_pts());
        buf
    }
    fn close_fd(&self) {
        unsafe {
            std::fs::File::from_raw_fd(self.fd);
        }
    }
    fn get_video_info(&self) -> VideoInfo {
        VideoInfo::new(VideoFormat::Bgrx, self.width, self.height)
            .build()
            .unwrap()
    }
}

struct Frames<'a> {
    events: &'a Receiver<FrameEvent>,
    evq: &'a mut EventQueue,
}

impl<'a> Iterator for Frames<'a> {
    type Item = FrameData;

    fn next(&mut self) -> Option<FrameData> {
        let mut frame_ready = false;
        let mut data = FrameData::default();
        loop {
            match self.events.try_recv() {
                Ok(FrameEvent::Frame { width, height, .. }) => {
                    data.width = width;
                    data.height = height;
                }
                Ok(FrameEvent::Ready {
                    tv_sec_hi,
                    tv_sec_lo,
                    tv_nsec,
                }) => {
                    data.tv_sec_hi = tv_sec_hi;
                    data.tv_sec_lo = tv_sec_lo;
                    data.tv_nsec = tv_nsec;
                    frame_ready = true;
                    break;
                }
                Ok(FrameEvent::Object { fd, size, .. }) => {
                    data.fd = fd;
                    data.size = size as usize;
                }
                Ok(FrameEvent::Cancel { .. }) => {
                    break;
                }
                Err(TryRecvError::Empty) => {
                    self.evq.dispatch().unwrap();
                }
                Err(TryRecvError::Disconnected) => {
                    break;
                }
                _ => {}
            }
        }
        if frame_ready {
            Some(data)
        } else {
            if data.fd != RawFd::default() {
                data.close_fd();
            }
            None
        }
    }
}

fn capture_frame(
    display: &Display,
    output: &WlOutput,
    frame_mgr: &ZwlrExportDmabufManagerV1,
    sender: Sender<FrameEvent>,
) {
    frame_mgr
        .capture_output(1, &output, move |newp| {
            newp.implement_closure(
                move |msg, _: ZwlrExportDmabufFrameV1| sender.send(msg).unwrap(),
                0usize,
            )
        })
        .unwrap();
    display.flush().unwrap();
}

fn main() {
    gstreamer::init().unwrap();
    let (display, mut evq) = Display::connect_to_env().unwrap();
    let global_mgr = GlobalManager::new(&display);

    display.flush().unwrap();
    evq.dispatch().unwrap();

    let output: WlOutput = global_mgr
        .instantiate_exact(1, |newp| newp.implement_dummy())
        .unwrap();
    let frame_mgr: ZwlrExportDmabufManagerV1 = global_mgr
        .instantiate_exact(1, |newp| newp.implement_dummy())
        .unwrap();

    let (events_sender, events_recv) = unbounded();

    let caps = {
        capture_frame(&display, &output, &frame_mgr, events_sender.clone());
        let mut frames = Frames {
            events: &events_recv,
            evq: &mut evq,
        };
        let data = frames.next().unwrap();
        data.close_fd();
        data.get_video_info().to_caps()
    };

    let need_data_cb = AppSrcCallbacks::new().need_data(move |appsrc, _| {
        let mut evq = display.create_event_queue();
        let frame_mgr = frame_mgr.as_ref().make_wrapper(&evq.get_token()).unwrap();

        capture_frame(&display, &output, &frame_mgr, events_sender.clone());

        let mut frames = Frames {
            events: &events_recv,
            evq: &mut evq,
        };
        if let Some(data) = frames.next() {
            let alloc = unsafe { gst_dmabuf_allocator_new() };
            let _ = appsrc.push_buffer(data.to_gst_buffer(alloc)).unwrap();
        }
    });

    let pipeline_str = "appsrc name=src ! vaapipostproc ! vaapih264enc dct8x8=true cabac=true ! matroskamux ! filesink location=stream.mkv";

    let pipeline: Pipeline = gstreamer::parse_launch(pipeline_str)
        .unwrap()
        .dynamic_cast()
        .unwrap();

    let appsrc: AppSrc = pipeline.get_by_name("src").unwrap().dynamic_cast().unwrap();
    appsrc.set_callbacks(need_data_cb.build());
    appsrc.set_caps(caps.as_ref());
    appsrc.set_property_format(gstreamer::Format::Time);

    pipeline.set_state(gstreamer::State::Playing).unwrap();
    let bus = pipeline.get_bus().unwrap();
    while let Some(_msg) = bus.timed_pop(gstreamer::CLOCK_TIME_NONE) {}

    pipeline.set_state(gstreamer::State::Null).unwrap();
}

mod generated {
    #![allow(dead_code, non_camel_case_types, unused_unsafe, unused_variables)]
    #![allow(non_upper_case_globals, non_snake_case, unused_imports)]

    pub mod client {
        pub(crate) use wayland_client::{
            protocol::{wl_output, wl_region, wl_surface},
            *,
        };
        pub(crate) use wayland_commons::{map::*, wire::*};
        include!(concat!(env!("OUT_DIR"), "/wlr_export_dmabuf_api.rs"));
    }
}
